package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import static java.lang.Math.sin;
import static java.lang.Math.cos;

public class Controller {
    @FXML
    private Line line_hour;
    @FXML
    private Line line_minute;
    @FXML
    private Line line_second;
    @FXML
    private Text text_date;
    @FXML
    private Text text_time;
    @FXML
    private Button btn_start;
    @FXML
    private Button btn_close;
    private  boolean clockState = false;//时钟状态
    //当前的时间
    private  Calendar dateTime;
    private int hours;
    private int minutes;
    private int seconds;


    private static SimpleDateFormat dateDF = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat timeDF = new SimpleDateFormat("HH:mm:ss");

    private static double centerX = 0;
    private static double centerY = 0;
    private static double hourLength = 60;
    private static double minuteLength = 75;
    private static double secondLength = 100;

    private static double p2i = 6.28;


    public void startClock() {
        clockState = true;
        clock();
    }

    public void closeClock() {
        clockState = false;
        clock();
    }


    private void setTime() {
        this.dateTime = Calendar.getInstance();
        this.hours = this.dateTime.get(Calendar.HOUR);
        this.minutes = this.dateTime.get(Calendar.MINUTE);
        this.seconds = this.dateTime.get(Calendar.SECOND);
    }

    //更新Text文本框的日期时间
    private void setTextDateTime() {
        text_time.setText("時間: " + timeDF.format(dateTime.getTime()));
        text_date.setText("日期: " + dateDF.format(dateTime.getTime()));
    }


    private void showButton() {
        btn_close.setVisible(clockState);
        btn_start.setVisible(!clockState);
    }


    private void setClockPointer() {
        double hourX = centerX + hourLength * sin((hours + minutes / 60)* p2i / 12);
        double hourY= centerY - hourLength * cos((hours + minutes / 60) * p2i / 12);
        double minuteX = centerX + minuteLength * sin(minutes * p2i / 60);
        double minuteY = centerY - minuteLength * cos(minutes * p2i / 60);
        double secondX = centerX + secondLength * sin(seconds * p2i / 60);
        double secondY = centerY - secondLength * cos(seconds * p2i / 60);
        line_hour.setEndX(hourX);
        line_hour.setEndY(hourY);
        line_minute.setEndX(minuteX);
        line_minute.setEndY(minuteY);
        line_second.setEndX(secondX);
        line_second.setEndY(secondY);
    }


    private void clock() {
        showButton();
        if (clockState)
            new Thread(() -> {
                try {
                    while (clockState) {
                        setTime();//刷新当前时间
                        setTextDateTime();//刷新文本框内容
                        setClockPointer();//刷新指针的位置
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
    }

}
